<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- transforms an OAI-PMH request to a GetRecords request or directly to a response, if possible (e.g. errors) -->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:ogc="http://www.opengis.net/ogc"
                xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" xmlns:gmd="http://www.isotc211.org/2005/gmd"
        exclude-result-prefixes="ogc csw">

    <xsl:import href="oai-pmhUtil.xsl"/>

    <xsl:output method="xml"/>

    <!-- request parameters -->
    <xsl:param name="verb"/>
    <xsl:param name="identifier"/>
    <xsl:param name="metadataPrefix"/>
    <xsl:param name="from"/>
    <xsl:param name="until"/>
    <xsl:param name="set"/>
    <xsl:param name="resumptionToken"/>

    <xsl:param name="response_date"/>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="$verb = 'ListSets'">
                <xsl:call-template name="listSets"/>
            </xsl:when>
            <xsl:when test="$verb = 'ListRecords' or $verb = 'ListIdentifiers'">
                <xsl:call-template name="listRecords"/>
            </xsl:when>
            <xsl:when test="$verb = 'GetRecord'">
                <xsl:call-template name="getRecord"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="error">
                    <xsl:with-param name="errorMessage">Bad or missing verb</xsl:with-param>
                    <xsl:with-param name="errorCode">badVerb</xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="listSets">
        <xsl:call-template name="error">
            <xsl:with-param name="errorMessage">Sets not supported</xsl:with-param>
            <xsl:with-param name="errorCode">noSetHierarchy</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="getRecord">
        <xsl:choose>
            <xsl:when test="$metadataPrefix != $prefix_dcat and $metadataPrefix != $prefix_geodcat and $metadataPrefix != $prefix_iso19139 and $metadataPrefix != $prefix_oai">
                <xsl:call-template name="error">
                    <xsl:with-param name="errorCode">badArgument</xsl:with-param>
                    <xsl:with-param name="errorMessage">Bad argument: metadataPrefix</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="not($identifier)">
                <xsl:call-template name="error">
                    <xsl:with-param name="errorCode">badArgument</xsl:with-param>
                    <xsl:with-param name="errorMessage">Missing argument: identifier</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <parameters>
                    <parameter>
                        <name>identifier</name>
                        <value><xsl:value-of select="$identifier"/></value>
                    </parameter>
                </parameters>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="listRecords">
        <xsl:choose>
            <xsl:when test="not($resumptionToken) and $metadataPrefix != $prefix_dcat and $metadataPrefix != $prefix_oai and $metadataPrefix != $prefix_iso19139 and $metadataPrefix != $prefix_geodcat">
                <xsl:call-template name="error">
                    <xsl:with-param name="errorCode">cannotDisseminateFormat</xsl:with-param>
                    <xsl:with-param name="errorMessage">Cannot disseminate format specified by metadataPrefix</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$set">
                <xsl:call-template name="error">
                    <xsl:with-param name="errorCode">noSetHierarchy</xsl:with-param>
                    <xsl:with-param name="errorMessage">Sets not supported</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$from or $until">
                <xsl:call-template name="error">
                    <xsl:with-param name="errorCode">noRecordsMatch</xsl:with-param>
                    <xsl:with-param name="errorMessage">The target catalog has no matching records</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$resumptionToken">
                <xsl:variable name="startPosition" select="number(substring-before($resumptionToken, ':'))"/>
                <xsl:variable name="tokenPrefix" select="substring-before(substring-after($resumptionToken, ':'), ':')"/>
                <xsl:variable name="startPage" select="substring-before(substring-after(substring-after($resumptionToken, $token_sep), $token_sep), $token_sep)"/>
                <xsl:choose>
                    <xsl:when test="string($startPosition) = 'NaN' or $startPosition &lt; 1">
                        <xsl:call-template name="badResumptionToken"/>
                    </xsl:when>
                    <xsl:when test="$tokenPrefix != $prefix_oai and $tokenPrefix != $prefix_dcat and $tokenPrefix != $prefix_iso19139 and $tokenPrefix != $prefix_geodcat">
                        <xsl:call-template name="badResumptionToken"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="parameters">
                            <xsl:with-param name="startPosition" select="$startPosition"/>
                            <xsl:with-param name="startPage" select="$startPage"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="parameters"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="badResumptionToken">
        <xsl:call-template name="error">
            <xsl:with-param name="errorCode">badResumptionToken</xsl:with-param>
            <xsl:with-param name="errorMessage" select="concat('Bad resumptionToken: ', $resumptionToken)"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="parameters">
        <xsl:param name="startPosition">1</xsl:param>
        <xsl:param name="startPage">1</xsl:param>
        <parameters>
            <parameter>
                <name>startIndex</name>
                <value><xsl:value-of select="$startPosition"/></value>
            </parameter>
            <parameter>
                <name>startPage</name>
                <value><xsl:value-of select="$startPage"/></value>
            </parameter>
        </parameters>
    </xsl:template>

</xsl:stylesheet>
