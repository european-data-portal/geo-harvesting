<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- removes one of the type filters -->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ogc="http://www.opengis.net/ogc"
        exclude-result-prefixes="ogc">

    <xsl:output method="xml"/>

    <xsl:template match="ogc:PropertyIsEqualTo[ogc:Literal/text() = 'datasetCollection']"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
