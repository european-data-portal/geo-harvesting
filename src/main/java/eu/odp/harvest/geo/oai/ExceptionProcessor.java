package eu.odp.harvest.geo.oai;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.http.HttpOperationFailedException;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Handles Exceptions for the OAI-PMH adapter.
 */
public class ExceptionProcessor implements Processor {

    private final static Logger LOG = Logger.getLogger(HarvesterManager.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        Exception e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
        if (e == null) {
            return;
        }
        Message out = exchange.getOut();
        Message in = exchange.getIn();
        out.setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
        out.setHeader(Exchange.CONTENT_TYPE, "text/plain");
        StringBuilder response = new StringBuilder();
        response.append(new Date()).append(" error during harvesting request to ")
                .append(in.getHeader("CamelHttpUri")).append("\n");
        if (e instanceof HttpOperationFailedException) {
            HttpOperationFailedException httpe = (HttpOperationFailedException)e;
            response.append("\nThe HTTP status of the response was: " + httpe.getStatusCode()).append("\n");
        }
        response.append("\nStacktrace:\n");
        addExcpetion(response, e);
        String harvestingCswRequest = in.getHeader("harvestingCswRequest", String.class);
        if (harvestingCswRequest != null) {
            response.append("\n\nGetRecords request sent to CSW:\n").append(harvestingCswRequest);
        }
        try {
            String body = in.getBody(java.lang.String.class);
            if (body != null) {
                response.append("\n\n\nCamel message body:\n").append(body);
            }
        }
        catch (Exception e2) {
            LOG.error("Error extracting message body", e2);
        }
        out.setBody(response.toString());
    }

    private void addExcpetion(StringBuilder response, Throwable e) {
        response.append(e.toString()).append("\n");
        for (StackTraceElement ste : e.getStackTrace()) {
            response.append(ste).append("\n");
        }
        Throwable cause = e.getCause();
        if (cause != null) {
            response.append("Caused by:\n");
            addExcpetion(response, cause);
        }
    }
}
