# Geo-Harvesting component for the Euro Open Data Portal

## Overview

* Provides an OAI-PMH interface to harvest ISO 19139 metadata from CSW (INSPIRE catalogs) and return it in DCAT-AP schema
* Deployed as a web application in Java servlet container
* This initial version supports three catalogs: GDI-DE, IDEE, EUMETSAT Product Navigator
* Provides a simple REST-like interface to manage harvester instances
* Implementation is based on Apache Camel

## Requirements

* JRE 7 or 8
* Tomcat 7 or 8

## Building with Apache Maven

1. cd to the root folder of this project (the folder that contains the pom.xml and this readme)
2. from the command line run

        > mvn package

## Deployment

Use your preferred method to deploy the webapp in Tomcat, e.g.:

* Copy the war file to the Tomcat webapps folder
* Create a context file in the Tomcat host folder

## Configuration

Logging can be configured with the log4j framework (see http://logging.apache.org/log4j/1.2/).
By default a logfile is created here: tomcat/logs/oai-pmh-geo-harvest.log.

To create a new harvester database for this application, use the script database-schema.sql in the project
root folder.

If you build with the env-dev profile, you can set your parameters during build. Please check the pom.xml
to see how parameters are set. The database parameters can be changed after deployment in the file
camel-oai-pmh.properties. The available parameters are:

* harvest.db.driver: JDBC driver class of the database
* harvest.db.url: JDBC URL of the database
* harvest.db.username: name of the database user
* harvest.db.password: Password of the database user

Note on HTTPS: There are a few catalogs that use HTTPS connections. However, some use self-signed certificates, or
certificates from a CA that is not trusted by the JVM per default. In order to allow integration of such catalogs,
the Geo-Harvesting trusts all server certificates. Of course this is insecure, as it makes the harvester vulnerable
to man-in-the-middle attacks. But the same is true for catalogs that are connected via plain HTTP (ca. 90% of
catalogs), so this vulnerability is inherent as long as HTTP connections are allowed.
If you require trusted connections via HTTPS, just remove the bean
eu.odp.harvest.geo.oai.http.AllowAllHttpClientConfig from the Apache Camel Spring configuration
(/WEB-INF/classes/camel-oai-pmh.xml).

## Compliance
This interface tries to comply with the GeoDCAT-AP specification. However, services are mapped in a different fashion
than specified by GeoDCAT-AP extended. It is expected that standardized resource access based on a service interface
will be part of a future baseline DCAT-AP version, and not solved on the level of GeoDCAT-AP.

## Usage

### OAI-PMH

Each harvester is exposed by a distinct HTTP endpoint. The endpoints are reached with this URL pattern:

        <tomcat-base-url><webapp-path>/harvesters/<endpoint>

So for example if tomcat-base-url is "http://localhost:8080", webapp-path is "/oai-pmh-geo-harvest-1.0.0-SNAPSHOT" and
you have a harvester "gdi-de" for the German GDI-DE portal, you can reach it with this URL:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/harvesters/gdi-de

You can use the management REST-like interface to query the available harvesters, including a short description and
the URL of the target catalog. You can also use the management REST interface to create new harvesters for any of the
available harvester types. For more information, see the section about the management REST interface below.

You can issue OAI-PMH requests to all of the available endpoints. All endpoints support the same set of operations.

Supported operations:
* ListRecords
* ListIdentifiers
* GetRecord

Unsupported feature:
* Sets
* Deleted records

Features supported only by a subset of endpoints:
* Selective harvesting with from/until parameters

Each endpoint supports two different values for the OAI-PMH metadataPrefix parameter in ListRecords and GetRecord:
* dcat_ap: The response only contains elements in the GeoDCAT-AP core profile.
* geodcat_ap_extended: The response also contains additional elements from the INSPIRE metadata for which no DCAT-AP
mapping exists, following the GeoDCAT-AP ectended profile.

#### Example requests

For the examples we assume the tomcat-base-url and webapp-path from the previous section. All requests can be
executed via HTTP GET method so it is possible to simply enter them in a Web browser.

To get the first set of records in DCAT-AP schema from the pre-configured GDI-DE catalog:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/harvesters/gdi-de?verb=ListRecords&metadataPrefix=dcat_ap

To get the first set of records in DCAT-AP schema from the pre-configured EUMETSAT Product Navigator that were updated after the 10th of March, 2015:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/harvesters/pn?verb=ListRecords&metadataPrefix=dcat_ap&from=2015-03-10

To iterate over the next set of records from the pre-configured IDEE catalog after an incomplete list was received from a previous request:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/harvesters/idee?verb=ListRecords&resumptionToken=<resumption-token-from-previous-request>

## Management REST interface

This interface allows to query, create, update and delete the harvester instances, plus query the available harvester types.
The available harvester types can only be queried. creation of a new harvester type must be done directly in the camel configuration.
The following http methods are used by the management interface:
* GET: get the available resources
* POST: create a new resource
* PUT: update an existing resource
* DELETE: delete an existing resource
The HTTP response code returned by the management interface tells the client if the request was successful:
* 200: Request succeeded
* Anything else: signals the kind of error that occured (e.g. 400 if request is invalid)

### Harvester types

The REST URL for harvester types is: <tomcat-base-url><webapp-path>/mgmt/harvester-types. A harvester type defines
which kind of metadata catalog can be harvested by harvesters of this type. E.g. there can be a harvester type for
INSPIRE catalogs, one type for CSW 3.0 OpenSearch, and so on.
Harvester types have the following parameters:
* id: ID of this harvester type. This ID is specified when creating a new instance of this harvester type (see below).
* name: Name of this harvester type.
* description: Any descrptive text.
The only supported method for harvester types is GET. Creation/update/deletion of harvester types must be done directly
inside the Apache Camel configuration.

Example request:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/mgmt/harvester-types

Example response:

        <harvesterTypes>
            <harvesterType>
                <id>inspire</id>
                <name>INSPIRE (plain XML)</name>
                <description>Harvester type for INSPIRE catalogs (via CSW AP-ISO profile plain HTTP/POST)</description>
            </harvesterType>
            <harvesterType>
                <id>inspireSoap11</id>
                <name>INSPIRE SOAP 1.1</name>
                <description>Harvester type for INSPIRE catalogs (via CSW AP-ISO profile SOAP 1.1)</description>
            </harvesterType>
            <harvesterType>
                <id>inspireSoap12</id>
                <name>INSPIRE SOAP 1.2</name>
                <description>Harvester type for INSPIRE catalogs (via CSW AP-ISO profile SOAP 1.2)</description>
            </harvesterType>
            <harvesterType>
                <id>esaEopos</id>
                <name>ESA EO OpenSearch</name>
                <description>Harvester type for Earth Observation Product catalogs via OpenSearch</description>
            </harvesterType>
        </harvesterTypes>

### Harvesters

The REST URL for harvesters is: <tomcat-base-url><webapp-path>/mgmt/harvesters. A harvester provides access to a specific
catalog instance. A harvester has the following properties:
* id: This is the ID of the harvester and part of the HTTP endpoint that is used ro access it. A harvester requires a unique ID.
As the ID of the harvester will be a part of the endpoint (URL) of the new harvester, please use only characters in
the new ID that are valid in a URL path.
* endpoint: The absolute URL of the Camel HTTP endpoint for this harvester. This endpoint is generated automatically by
the application based on the base URL of the web application and the id, so does not have to be specified in insert/update
requests.
* name: Name of this harvester.
* description: Any descriptive text for this harvester.
* type: The type of the catalog that should be harvested by this harvester. The value is the ID of the harvesterType
from the section above.
* url: URL of the target catalog to be harvested that accepts retrieval requests. For INSPIRE catalogs, this is the URL
that accepts GetRecords requests. For EOPOS catalogs, this is the URL template that allows iteration over the records
with startPage or startIndex.
* selective: Flag that signals if selective (incremental) harvesting of the catalog is possible. If true, the target catalog
must support a temporal filter that allows to query only records updated in a specific time interval with the from/until
parameters in the OAI-PMH ListRecords/ListIdentifiers requests.
* responsibleParty: the organisation that is reponsible for the target catalog
* email: email of the contact person for the target catalog
* homepage: homepage of the portal that the target catalog is a part of
* language: default language of the catalog, as ISO 639-1 code
* country: country of the target catalog, as 2-digit European code

#### Query harvesters
HTTP method: GET.

##### Query all available harvesters

Example request:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/mgmt/harvesters

Example response:

        <harvesters>
           <harvester>
           ...
           </harvester>
           <harvester>
           ...
           </harvester>
           <harvester>
           ...
           </harvester>
        </harvesters>

##### Query a specific harvester
The ID of the harvester to delete is specified as last part of the URL path.

Example request:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/mgmt/harvesters/pn

Example response:

        <harvester>
            <id>pn</id>
            <endpoint>http://localhost:8082/oai-pmh/harvesters/pn</endpoint>
            <type>inspire</type>
            <name>EUMETSAT Product Navigator</name>
            <description>Meteo Data Portal Europe</description>
            <url>https://navigator.eumetsat.int/elastic-csw/service</url>
            <selective>true</selective>
            <responsibleParty>European Organisation for the Exploitation of Meteorological Satellites (EUMETSAT)</responsibleParty>
            <email>press@eumetsat.int</email>
            <homepage>http://navigator.eumetsat.int</homepage>
            <language>en</language>
        </harvester>

#### Create a new harvester
HTTP method: POST. This request uses an HTTP request body of content-type application/xml that defines the new harvester.
Note that it is not necessary to specify an endpoint, since this will be assigned by the application based on the id.

Example request:

        <harvester>
            <id>pn</id>
            <type>inspire</type>
            <name>EUMETSAT Product Navigator</name>
            <description>Meteo Data Portal Europe</description>
            <url>https://navigator.eumetsat.int/elastic-csw/service</url>
            <selective>true</selective>
            <responsibleParty>European Organisation for the Exploitation of Meteorological Satellites (EUMETSAT)</responsibleParty>
            <email>press@eumetsat.int</email>
            <homepage>http://navigator.eumetsat.int</homepage>
            <language>en</language>
        </harvester>

#### Update an existing harvester
HTTP method: PUT. This request uses an HTTP request body of content-type application/xml that defines the new harvester
properties.

Example request:

        <harvester>
            <id>pn2</id>
            <type>inspire</type>
            <name>EUMETSAT Product Navigator</name>
            <description>Meteo Data Portal Europe</description>
            <url>https://navigator.eumetsat.int/elastic-csw/service</url>
            <selective>true</selective>
            <responsibleParty>European Organisation for the Exploitation of Meteorological Satellites (EUMETSAT)</responsibleParty>
            <email>press@eumetsat.int</email>
            <homepage>http://navigator.eumetsat.int</homepage>
            <language>en</language>
        </harvester>

#### Delete an existing harvester
HTTP method: DELETE. The ID of the harvester to delete is specified as last part of the URL path.

Example request:

        http://localhost:8080/oai-pmh-geo-harvest-1.0.0-SNAPSHOT/mgmt/harvesters/pn2

## Further Reading

* http://www.openarchives.org/OAI/openarchivesprotocol.html
* http://www.w3.org/TR/vocab-dcat/
* https://joinup.ec.europa.eu/asset/dcat_application_profile/description
* https://joinup.ec.europa.eu/asset/dcat_application_profile/asset_release/geodcat-ap-v10
* http://camel.apache.org/

